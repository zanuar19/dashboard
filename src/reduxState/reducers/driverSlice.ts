import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'reduxState/store';
import { paginate, searchData } from 'helpers/helpers';
import { PAGE_SIZE, STATUS_FETCH } from 'helpers/constants';
import { fetchingDriver } from 'reduxState/actions/driver';

interface Name {
  title: string;
  first: string;
  last: string;
}

interface Street {
  number: number;
  name: string;
}

interface Coordinates {
  latitude: string;
  longitude: string;
}

interface Timezone {
  offset: string;
  description: string;
}

interface Location {
  street: Street;
  city: string;
  state: string;
  country: string;
  postcode: string | number;
  coordinates: Coordinates;
  timezone: Timezone;
}

interface Login {
  uuid: string;
  username: string;
  password: string;
  salt: string;
  md5: string;
  sha1: string;
  sha256: string;
}

interface Dob {
  date: Date;
  age: number;
}

interface Registered {
  date: Date;
  age: number;
}

interface Id {
  name: string;
  value?: any;
}

interface Picture {
  large: string;
  medium: string;
  thumbnail: string;
}

export interface DriverData {
  gender: string;
  name: Name;
  location: Location;
  email: string;
  login: Login;
  dob: Dob;
  registered: Registered;
  phone: string;
  cell: string;
  id: Id;
  picture: Picture;
  nat: string;
}

export interface DriverState {
  data: Array<DriverData>;
  status: STATUS_FETCH.IDLE | STATUS_FETCH.PENDING | STATUS_FETCH.FAILED | STATUS_FETCH.RESOLVED;
  search: string;
  page: number;
  totalPage: number;
}

const initialState: DriverState = {
  data: [],
  status: STATUS_FETCH.IDLE,
  search: '',
  page: 1,
  totalPage: 1,
};

export const fetchDriver = createAsyncThunk(
  'driver/fetchDriver',
  async () => {
    const response = await fetchingDriver();
    return response
  }
);

export const driverSlice = createSlice({
  name: 'driver',
  initialState,
  reducers: {
    setSearch: (state, action: PayloadAction<string>) => {
      const data = searchData(state.data, action.payload)
      state.search = action.payload
      state.page = 1
      state.totalPage = Math.ceil(data.length / PAGE_SIZE)
    },
    setPage: (state, action: PayloadAction<number>) => {      
      state.page = action.payload
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchDriver.pending, (state) => {
        state.status = STATUS_FETCH.PENDING;
      })
      .addCase(fetchDriver.fulfilled, (state, action) => {                
        state.status = STATUS_FETCH.RESOLVED;
        state.data = action.payload;
        state.totalPage = Math.ceil(action.payload.length / PAGE_SIZE);
      });
  },
});

export const { setSearch, setPage } = driverSlice.actions;


export const selectDriver = (state: RootState): DriverState => {  
  const searchTerm = state.driver.search;
  if (!searchTerm) {
    return {
      ...state.driver,
      data: paginate(state.driver.data, state.driver.page)
    }
  }
  
  const data = searchData(state.driver.data, searchTerm)

  return {
    ...state.driver,
    data: paginate(data, state.driver.page)
  }
};

export const selectPage = (state: RootState) => state.driver.page
export const selectTotalPage = (state: RootState) => state.driver.totalPage
export default driverSlice.reducer;
