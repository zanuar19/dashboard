import { STATUS_FETCH } from 'helpers/constants';

import counterReducer, {
  setSearch,
  setPage,
  DriverState
} from '../driverSlice';

describe('counter reducer', () => {

  const resolvedState: DriverState = {
    data: [
      {
        gender: 'female',
        name: { title: 'Miss', first: 'Elya', last: 'Legrand' },
        location: {
          street: { number: 3828, name: 'Rue Bossuet' },
          city: 'Tourcoing',
          state: 'Ardennes',
          country: 'France',
          postcode: '88716',
          coordinates: { latitude: '-1.7591', longitude: '53.1060' },
          timezone: { offset: '-10:00', description: 'Hawaii' }
        },
        email: 'elya.legrand@example.com',
        login: {
          uuid: '480166e0-d56b-44ae-a509-28cd6f244345',
          username: 'purplecat590',
          password: '3636',
          salt: 'vATpN33t',
          md5: '6b682942b43856be7e3acb933ac7f9c7',
          sha1: '25511df2cef8943d8d782d6efd7f1aeea74e2a3b',
          sha256:
            'b16501771dc65b4202032018e09aebb136b2909ca04c6e0f5f89a085c4e4fd32'
        },
        dob: { date: new Date('1975-04-09T01:39:53.762Z'), age: 47 },
        registered: { date: new Date('2008-09-04T02:58:22.872Z'), age: 14 },
        phone: '05-13-21-33-20',
        cell: '06-59-12-33-77',
        id: { name: 'INSEE', value: '2NNaN46607361 27' },
        picture: {
          large: 'https://randomuser.me/api/portraits/women/42.jpg',
          medium: 'https://randomuser.me/api/portraits/med/women/42.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/women/42.jpg'
        },
        nat: 'FR'
      },
      {
        gender: 'male',
        name: { title: 'Mr', first: 'Owen', last: 'Addy' },
        location: {
          street: { number: 2819, name: 'Richmond Ave' },
          city: 'Trout Lake',
          state: 'Alberta',
          country: 'Canada',
          postcode: '',
          coordinates: { latitude: '-38.2059', longitude: '-148.3380' },
          timezone: {
            offset: '+5:00',
            description: 'Ekaterinburg, Islamabad, Karachi, Tashkent'
          }
        },
        email: 'owen.addy@example.com',
        login: {
          uuid: '9e1455bc-6b90-4b5b-a3f2-2b0a4ce03679',
          username: 'yellowbutterfly693',
          password: 'hand',
          salt: 'zDMi31NN',
          md5: '3a58766753722a82f6f3df36664097fb',
          sha1: '1d57c206dfa4009c395a9821640337f82384feee',
          sha256:
            '05900fef685cfe7d36e112c3c2443be139e89d9f56f997309c530c4cbf829cb6'
        },
        dob: { date: new Date('1975-04-09T01:39:53.762Z'), age: 47 },
        registered: { date: new Date('2008-09-04T02:58:22.872Z'), age: 14 },
        phone: '665-134-3802',
        cell: '127-287-5964',
        id: { name: '', value: null },
        picture: {
          large: 'https://randomuser.me/api/portraits/men/7.jpg',
          medium: 'https://randomuser.me/api/portraits/med/men/7.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/men/7.jpg'
        },
        nat: 'CA'
      },
      {
        gender: 'male',
        name: { title: 'Mr', first: 'Chad', last: 'Holmes' },
        location: {
          street: { number: 3022, name: 'Queens Road' },
          city: 'Newcastle upon Tyne',
          state: 'Cleveland',
          country: 'United Kingdom',
          postcode: 'VA92 0YW',
          coordinates: { latitude: '-65.8785', longitude: '139.3317' },
          timezone: {
            offset: '+9:00',
            description: 'Tokyo, Seoul, Osaka, Sapporo, Yakutsk'
          }
        },
        email: 'chad.holmes@example.com',
        login: {
          uuid: 'daa6f1a3-c1ab-4925-b6d0-324b4dc9fa6c',
          username: 'bigbird716',
          password: 'accord',
          salt: 'fibrDf63',
          md5: '2c56ab74a2285d212abc6c4b9330ade5',
          sha1: '4c67ba8b3b807253cf23b608d04c2a0e75239e96',
          sha256:
            '0392c0344a9c053014725e762104fc3f6441c31cb340e1d5e132a3306f97c9cd'
        },
        dob: { date: new Date('1975-04-09T01:39:53.762Z'), age: 47 },
        registered: { date: new Date('2008-09-04T02:58:22.872Z'), age: 14 },
        phone: '017683 39930',
        cell: '0750-704-586',
        id: { name: 'NINO', value: 'BE 89 06 92 C' },
        picture: {
          large: 'https://randomuser.me/api/portraits/men/83.jpg',
          medium: 'https://randomuser.me/api/portraits/med/men/83.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/men/83.jpg'
        },
        nat: 'GB'
      },
      {
        gender: 'male',
        name: { title: 'Mr', first: 'Harry', last: 'Wulf' },
        location: {
          street: { number: 9636, name: 'Talstraße' },
          city: 'Löhne',
          state: 'Hessen',
          country: 'Germany',
          postcode: 65538,
          coordinates: { latitude: '-10.9212', longitude: '-161.4235' },
          timezone: {
            offset: '+8:00',
            description: 'Beijing, Perth, Singapore, Hong Kong'
          }
        },
        email: 'harry.wulf@example.com',
        login: {
          uuid: '8b19726e-4a81-4312-8471-ee111a45d721',
          username: 'crazybird693',
          password: 'jubilee',
          salt: '7Ccxu9va',
          md5: '13a0253b2db6fc3f6fcea272abe76044',
          sha1: '3e188184040e6fbb1cd2546df20138718bb3e4dc',
          sha256:
            'a3acea18a8e1b6a9b2576203a187edd78ecf16f131ff573f64ff9a4d663499eb'
        },
        dob: { date: new Date('1975-04-09T01:39:53.762Z'), age: 47 },
        registered: { date: new Date('2008-09-04T02:58:22.872Z'), age: 14 },
        phone: '0369-7312823',
        cell: '0173-0425825',
        id: { name: '', value: null },
        picture: {
          large: 'https://randomuser.me/api/portraits/men/30.jpg',
          medium: 'https://randomuser.me/api/portraits/med/men/30.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/men/30.jpg'
        },
        nat: 'DE'
      },
      {
        gender: 'female',
        name: { title: 'Madame', first: 'Lucia', last: 'Lemoine' },
        location: {
          street: { number: 6146, name: "Rue de L'Abbé-Soulange-Bodin" },
          city: 'Genthod',
          state: 'Thurgau',
          country: 'Switzerland',
          postcode: 4625,
          coordinates: { latitude: '-88.9992', longitude: '-45.9027' },
          timezone: { offset: '-2:00', description: 'Mid-Atlantic' }
        },
        email: 'lucia.lemoine@example.com',
        login: {
          uuid: '893ee39b-5feb-4e7a-9285-fbe369e40c5c',
          username: 'smallmouse986',
          password: 'nnnnnnn',
          salt: 'xBdBtcxF',
          md5: 'e8f7a1c8b81e1afe5b632bd41e9b7fb1',
          sha1: '408389f1b20512bd64d5f12f5e3e516aa076161c',
          sha256:
            '86193dc04bafab6539977194d174a49e3db5c551e29f45c2abd9147cdbb2f31e'
        },
        dob: { date: new Date('1975-04-09T01:39:53.762Z'), age: 47 },
        registered: { date: new Date('2008-09-04T02:58:22.872Z'), age: 14 },
        phone: '079 447 77 85',
        cell: '075 767 42 36',
        id: { name: 'AVS', value: '756.1639.3356.59' },
        picture: {
          large: 'https://randomuser.me/api/portraits/women/33.jpg',
          medium: 'https://randomuser.me/api/portraits/med/women/33.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/women/33.jpg'
        },
        nat: 'CH'
      }
    ],
    status: STATUS_FETCH.RESOLVED,
    search: '',
    page: 1,
    totalPage: 6
  };

  it('should handle initial state', () => {
    expect(counterReducer(undefined, { type: 'unknown' })).toEqual({
      data: [],
      status: 'idle',
      search: '',
      page: 1,
      totalPage: 1
    });
  });

  const expectedSearchState = {
    search: 'elya',
    page: 1
  };

  it('should handle setSearch', () => {
    const actual = counterReducer(resolvedState, setSearch('elya'));
    expect(actual.search).toEqual(expectedSearchState.search);
  });

  it('should handle setPage', () => {
    const actual = counterReducer(resolvedState, setPage(1));
    expect(actual.page).toEqual(expectedSearchState.page);
  });
});
