import { API_HOST } from 'helpers/constants';
import { getStorage, setStorage } from 'helpers/helpers';

const fetchData = async () => {
  const response = await fetch(`${API_HOST}/api/?results=30`);
  const json = await response.json();
  return json.results;
};

export const fetchingDriver = async () => {
  if (getStorage().length) {
    return getStorage();
  }
  const response = await fetchData();
  setStorage(response);
  return response;
};
