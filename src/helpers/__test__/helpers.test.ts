import { searchData } from "../helpers";

const data = [{
  "gender": "female",
  "name": {
    "title": "Mrs",
    "first": "Lene",
    "last": "Orth"
  },
  "location": {
    "street": {
      "number": 234,
      "name": "Im Winkel"
    },
    "city": "Emmelshausen",
    "state": "Hamburg",
    "country": "Germany",
    "postcode": 83911,
    "coordinates": {
      "latitude": "-46.3401",
      "longitude": "124.0094"
    },
    "timezone": {
      "offset": "+4:00",
      "description": "Abu Dhabi, Muscat, Baku, Tbilisi"
    }
  },
  "email": "lene.orth@example.com",
  "login": {
    "uuid": "9a9a0635-d837-4bad-b8aa-ba4e4f6580d4",
    "username": "redpanda694",
    "password": "nancy",
    "salt": "IpETN39F",
    "md5": "e01a46af2c39851daf60f399282dc210",
    "sha1": "fec783300ec71aa9ef2257b24fa63175bb1aafbf",
    "sha256": "a309fde39e5e0d0ef6f0277bc3553736a9c9474df0912d74d58bd01098cf5ba4"
  },
  "dob": {
    "date": "1961-06-01T15:38:10.466Z",
    "age": 61
  },
  "registered": {
    "date": "2017-10-22T11:46:22.376Z",
    "age": 5
  },
  "phone": "0933-5082529",
  "cell": "0175-2310627",
  "id": {
    "name": "",
    "value": null
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/26.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/26.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/26.jpg"
  },
  "nat": "DE"
}, {
  "gender": "female",
  "name": {
    "title": "Ms",
    "first": "Minea",
    "last": "Lehtinen"
  },
  "location": {
    "street": {
      "number": 9047,
      "name": "Visiokatu"
    },
    "city": "Kitee",
    "state": "Kainuu",
    "country": "Finland",
    "postcode": 80866,
    "coordinates": {
      "latitude": "6.2393",
      "longitude": "-98.8400"
    },
    "timezone": {
      "offset": "+9:00",
      "description": "Tokyo, Seoul, Osaka, Sapporo, Yakutsk"
    }
  },
  "email": "minea.lehtinen@example.com",
  "login": {
    "uuid": "e84620a3-b789-47ee-af52-c1dce632dcd2",
    "username": "smalldog531",
    "password": "sidekick",
    "salt": "fZX4xnlZ",
    "md5": "47f33c494af6d8727dcc452657f6f967",
    "sha1": "c2c47f76626c4e061bcbaf3c48413c4cbee164d1",
    "sha256": "13c3d19b57f42129631869aaddcd0b15ed769ae1f32adbc9a16e0b7076478135"
  },
  "dob": {
    "date": "1987-01-17T13:05:47.580Z",
    "age": 35
  },
  "registered": {
    "date": "2016-01-08T17:13:26.628Z",
    "age": 6
  },
  "phone": "09-970-322",
  "cell": "041-017-98-78",
  "id": {
    "name": "HETU",
    "value": "NaNNA930undefined"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/75.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/75.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/75.jpg"
  },
  "nat": "FI"
}, {
  "gender": "female",
  "name": {
    "title": "Miss",
    "first": "Faith",
    "last": "Young"
  },
  "location": {
    "street": {
      "number": 6968,
      "name": "Ormond Quay"
    },
    "city": "Clane",
    "state": "Offaly",
    "country": "Ireland",
    "postcode": 63661,
    "coordinates": {
      "latitude": "-3.6667",
      "longitude": "102.1235"
    },
    "timezone": {
      "offset": "+9:00",
      "description": "Tokyo, Seoul, Osaka, Sapporo, Yakutsk"
    }
  },
  "email": "faith.young@example.com",
  "login": {
    "uuid": "cceeb054-4202-4ced-8df5-09b6f2eb94b9",
    "username": "yellowbird796",
    "password": "booster",
    "salt": "dU9xiy5m",
    "md5": "bbe3e6018bbd8e7f690eb0c33d647766",
    "sha1": "591562d92e2fb5d1609e5c04b83ad969c390279f",
    "sha256": "3a90cc2e9bae221571d7a7e0f5b8962a26fadbe931792944c0034ab8eef6a4b9"
  },
  "dob": {
    "date": "1967-10-06T05:02:42.332Z",
    "age": 55
  },
  "registered": {
    "date": "2008-05-04T04:19:00.059Z",
    "age": 14
  },
  "phone": "051-173-9297",
  "cell": "081-626-8284",
  "id": {
    "name": "PPS",
    "value": "5714293T"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/79.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/79.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/79.jpg"
  },
  "nat": "IE"
}, {
  "gender": "male",
  "name": {
    "title": "Mr",
    "first": "Soham",
    "last": "Ray"
  },
  "location": {
    "street": {
      "number": 9991,
      "name": "Lovers Ln"
    },
    "city": "Orange",
    "state": "Northern Territory",
    "country": "Australia",
    "postcode": 2946,
    "coordinates": {
      "latitude": "-78.7081",
      "longitude": "89.4488"
    },
    "timezone": {
      "offset": "0:00",
      "description": "Western Europe Time, London, Lisbon, Casablanca"
    }
  },
  "email": "soham.ray@example.com",
  "login": {
    "uuid": "f526e32f-1c65-4da2-86f3-eba3ad3d68c1",
    "username": "yellowwolf139",
    "password": "ninguna",
    "salt": "1MunXxi2",
    "md5": "8ccae28976e075efed4e8dc6fba91241",
    "sha1": "eaa588ffd1e488bc625499179408e5ee657cfc8c",
    "sha256": "1fb2ad11a1b0d3764ab81059dd8b75404c6eb2a100d8b1a4af2e3a30ca745838"
  },
  "dob": {
    "date": "1974-02-18T15:35:17.714Z",
    "age": 48
  },
  "registered": {
    "date": "2014-09-15T08:37:45.708Z",
    "age": 8
  },
  "phone": "08-9106-6035",
  "cell": "0472-976-204",
  "id": {
    "name": "TFN",
    "value": "378953223"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/men/88.jpg",
    "medium": "https://randomuser.me/api/portraits/med/men/88.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/men/88.jpg"
  },
  "nat": "AU"
}, {
  "gender": "female",
  "name": {
    "title": "Mrs",
    "first": "Cremilda",
    "last": "da Cunha"
  },
  "location": {
    "street": {
      "number": 9088,
      "name": "Rua Vinte E Quatro de Outubro"
    },
    "city": "Barreiras",
    "state": "Roraima",
    "country": "Brazil",
    "postcode": 98911,
    "coordinates": {
      "latitude": "-15.8772",
      "longitude": "-77.3503"
    },
    "timezone": {
      "offset": "-1:00",
      "description": "Azores, Cape Verde Islands"
    }
  },
  "email": "cremilda.dacunha@example.com",
  "login": {
    "uuid": "f503eb7f-7a48-4d95-bc62-dafe08bede30",
    "username": "bigsnake179",
    "password": "thewho",
    "salt": "w6QVLcqI",
    "md5": "c0547bcac0c3c59dc738c2b0213286c0",
    "sha1": "94ea463e71d8d2a374f07ba60c3205085d73c330",
    "sha256": "849343cd44b87e0e0649ceee647cedeb7e775af3a558ceccbf6f061584b1cb01"
  },
  "dob": {
    "date": "1990-04-06T06:00:25.133Z",
    "age": 32
  },
  "registered": {
    "date": "2014-04-24T11:30:46.931Z",
    "age": 8
  },
  "phone": "(69) 6236-5674",
  "cell": "(87) 8665-7933",
  "id": {
    "name": "",
    "value": null
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/86.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/86.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/86.jpg"
  },
  "nat": "BR"
}, {
  "gender": "female",
  "name": {
    "title": "Miss",
    "first": "Iracema",
    "last": "Fernandes"
  },
  "location": {
    "street": {
      "number": 2503,
      "name": "Rua da Paz "
    },
    "city": "Petrolina",
    "state": "Bahia",
    "country": "Brazil",
    "postcode": 78397,
    "coordinates": {
      "latitude": "35.3506",
      "longitude": "71.4972"
    },
    "timezone": {
      "offset": "+3:00",
      "description": "Baghdad, Riyadh, Moscow, St. Petersburg"
    }
  },
  "email": "iracema.fernandes@example.com",
  "login": {
    "uuid": "058c4eca-3174-4cde-9633-540d2476ef1d",
    "username": "whiteostrich331",
    "password": "gamma",
    "salt": "1nYCXzLS",
    "md5": "bafa4a254acab00845b3d521624b3cb9",
    "sha1": "e8b4c7ddc58bbd2ab16b7be63232e5e6c0b64d1d",
    "sha256": "8f2d2533e22d0437d0a8ce7bf11a32f76cfc880662a272944aa09f67668e72a1"
  },
  "dob": {
    "date": "1979-12-16T20:18:37.730Z",
    "age": 43
  },
  "registered": {
    "date": "2009-10-28T12:30:52.069Z",
    "age": 13
  },
  "phone": "(57) 4346-2858",
  "cell": "(61) 6883-8947",
  "id": {
    "name": "",
    "value": null
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/20.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/20.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/20.jpg"
  },
  "nat": "BR"
}, {
  "gender": "male",
  "name": {
    "title": "Mr",
    "first": "Reinhardt",
    "last": "Dreyer"
  },
  "location": {
    "street": {
      "number": 2573,
      "name": "Schützenstraße"
    },
    "city": "Adenau",
    "state": "Niedersachsen",
    "country": "Germany",
    "postcode": 91176,
    "coordinates": {
      "latitude": "87.8239",
      "longitude": "-52.1140"
    },
    "timezone": {
      "offset": "+9:00",
      "description": "Tokyo, Seoul, Osaka, Sapporo, Yakutsk"
    }
  },
  "email": "reinhardt.dreyer@example.com",
  "login": {
    "uuid": "eac9048e-1d1d-4bea-aa45-b8ab6dbeaa9b",
    "username": "lazygoose824",
    "password": "tennis1",
    "salt": "Q3y1ZeHp",
    "md5": "e8421978889c9f0b376cfc7b5bd27b3d",
    "sha1": "49bba307ab6f53ab722cf414d9c71769a0fc9552",
    "sha256": "1315bb27f8d541b93a2001caeeaddcd4a416fe04ca7e615d48b0a92ddf658883"
  },
  "dob": {
    "date": "1958-03-01T21:56:15.711Z",
    "age": 64
  },
  "registered": {
    "date": "2008-05-02T11:33:04.567Z",
    "age": 14
  },
  "phone": "0978-0081969",
  "cell": "0173-5002696",
  "id": {
    "name": "",
    "value": null
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/men/74.jpg",
    "medium": "https://randomuser.me/api/portraits/med/men/74.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/men/74.jpg"
  },
  "nat": "DE"
}, {
  "gender": "male",
  "name": {
    "title": "Mr",
    "first": "Kuzey",
    "last": "Eronat"
  },
  "location": {
    "street": {
      "number": 537,
      "name": "Anafartalar Cd"
    },
    "city": "Ardahan",
    "state": "Ordu",
    "country": "Turkey",
    "postcode": 47462,
    "coordinates": {
      "latitude": "80.0760",
      "longitude": "-66.3672"
    },
    "timezone": {
      "offset": "-2:00",
      "description": "Mid-Atlantic"
    }
  },
  "email": "kuzey.eronat@example.com",
  "login": {
    "uuid": "51e66484-3a2c-4d3c-bc69-b6fb4a639b64",
    "username": "bluefish130",
    "password": "hawkeyes",
    "salt": "0CSDNH37",
    "md5": "67788af2fb23a87116badbd81e56be25",
    "sha1": "93b6b5ab339c0475e2e678b8e557a94573c4ff7e",
    "sha256": "a0d2f333fdf688e49968820dee182d344e2a56ec50263c14be75942d0ea0596f"
  },
  "dob": {
    "date": "1949-12-12T03:30:11.730Z",
    "age": 73
  },
  "registered": {
    "date": "2012-03-11T02:07:07.420Z",
    "age": 10
  },
  "phone": "(224)-623-5841",
  "cell": "(170)-350-8004",
  "id": {
    "name": "",
    "value": null
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/men/71.jpg",
    "medium": "https://randomuser.me/api/portraits/med/men/71.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/men/71.jpg"
  },
  "nat": "TR"
}, {
  "gender": "female",
  "name": {
    "title": "Mrs",
    "first": "Alice",
    "last": "Payne"
  },
  "location": {
    "street": {
      "number": 7162,
      "name": "Dame Street"
    },
    "city": "Duleek",
    "state": "Meath",
    "country": "Ireland",
    "postcode": 11261,
    "coordinates": {
      "latitude": "-15.3623",
      "longitude": "36.2619"
    },
    "timezone": {
      "offset": "+4:00",
      "description": "Abu Dhabi, Muscat, Baku, Tbilisi"
    }
  },
  "email": "alice.payne@example.com",
  "login": {
    "uuid": "3d8db2ad-e4ef-46c4-9c2e-4e08eae6dd18",
    "username": "organicelephant588",
    "password": "altoids",
    "salt": "XFjdBFnk",
    "md5": "46e238f3283689dd5c337a9db6e42b97",
    "sha1": "2ca2424a33b5e74c2eecd7de89e33bc5d1221d0f",
    "sha256": "35d198a24d7320ffefe100afd267b4c6fc7814a3f1742544705b0f24ad4c7c37"
  },
  "dob": {
    "date": "1955-08-28T20:39:22.944Z",
    "age": 67
  },
  "registered": {
    "date": "2004-04-20T17:09:38.718Z",
    "age": 18
  },
  "phone": "031-916-8776",
  "cell": "081-494-1293",
  "id": {
    "name": "PPS",
    "value": "5302596T"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/36.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/36.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/36.jpg"
  },
  "nat": "IE"
}, {
  "gender": "female",
  "name": {
    "title": "Mrs",
    "first": "Joanna",
    "last": "Wohlfarth"
  },
  "location": {
    "street": {
      "number": 216,
      "name": "Römerstraße"
    },
    "city": "Emmendingen",
    "state": "Baden-Württemberg",
    "country": "Germany",
    "postcode": 15360,
    "coordinates": {
      "latitude": "-45.8021",
      "longitude": "140.1955"
    },
    "timezone": {
      "offset": "-11:00",
      "description": "Midway Island, Samoa"
    }
  },
  "email": "joanna.wohlfarth@example.com",
  "login": {
    "uuid": "85c35bfc-d01c-42fa-a590-03afae486601",
    "username": "redlion215",
    "password": "scrabble",
    "salt": "NJA8Lzgc",
    "md5": "2737cf73297d20b5519b49d739095585",
    "sha1": "72786039ebc543372c889ebb089efdaf9eb5f3b2",
    "sha256": "3d7802666895688df9a7f4ee43031a6e55195d819f6935e623eac4c1fad5160f"
  },
  "dob": {
    "date": "1963-02-20T13:21:37.754Z",
    "age": 59
  },
  "registered": {
    "date": "2015-06-14T00:24:30.350Z",
    "age": 7
  },
  "phone": "0311-3975708",
  "cell": "0171-9859857",
  "id": {
    "name": "",
    "value": null
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/38.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/38.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/38.jpg"
  },
  "nat": "DE"
}, {
  "gender": "male",
  "name": {
    "title": "Mr",
    "first": "Julian",
    "last": "Williamson"
  },
  "location": {
    "street": {
      "number": 7601,
      "name": "Hogan St"
    },
    "city": "Fort Lauderdale",
    "state": "Colorado",
    "country": "United States",
    "postcode": 70541,
    "coordinates": {
      "latitude": "77.4682",
      "longitude": "53.6540"
    },
    "timezone": {
      "offset": "-7:00",
      "description": "Mountain Time (US & Canada)"
    }
  },
  "email": "julian.williamson@example.com",
  "login": {
    "uuid": "6f7b7a8d-f634-4019-b347-a458c622b168",
    "username": "lazycat350",
    "password": "samm",
    "salt": "5fTK3AN5",
    "md5": "8f434ae0f7eb81318ffc05a3312202a7",
    "sha1": "c9bba41019926485d92468dad29a652eb30172c5",
    "sha256": "00f23d6cd1af108b4d6913bf030309a28661986dc7e98057e5a7ea3b1a571a07"
  },
  "dob": {
    "date": "1979-07-29T08:10:25.470Z",
    "age": 43
  },
  "registered": {
    "date": "2006-06-11T22:23:07.338Z",
    "age": 16
  },
  "phone": "(623)-563-9152",
  "cell": "(464)-054-4580",
  "id": {
    "name": "SSN",
    "value": "489-14-4843"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/men/68.jpg",
    "medium": "https://randomuser.me/api/portraits/med/men/68.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/men/68.jpg"
  },
  "nat": "US"
}, {
  "gender": "female",
  "name": {
    "title": "Ms",
    "first": "Amandine",
    "last": "Leclercq"
  },
  "location": {
    "street": {
      "number": 2000,
      "name": "Avenue Goerges Clémenceau"
    },
    "city": "Villeurbanne",
    "state": "Orne",
    "country": "France",
    "postcode": 78028,
    "coordinates": {
      "latitude": "-79.5412",
      "longitude": "124.9071"
    },
    "timezone": {
      "offset": "-5:00",
      "description": "Eastern Time (US & Canada), Bogota, Lima"
    }
  },
  "email": "amandine.leclercq@example.com",
  "login": {
    "uuid": "0079f0b6-9b15-4a0c-8207-83219383154f",
    "username": "happygorilla862",
    "password": "kahlua",
    "salt": "4ePGoecG",
    "md5": "c78f5f91b37d24ed582727adfeb4a99d",
    "sha1": "7891cc6f1d96345f3fa63c80e0d968c8ce375522",
    "sha256": "2185ace3affe9db2562e128c376b6df03eb90ee38546cd89bfd3740fbc5ace01"
  },
  "dob": {
    "date": "1982-05-17T18:13:32.585Z",
    "age": 40
  },
  "registered": {
    "date": "2004-08-20T20:54:04.076Z",
    "age": 18
  },
  "phone": "04-65-51-57-10",
  "cell": "06-45-37-72-70",
  "id": {
    "name": "INSEE",
    "value": "2NNaN73475434 55"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/54.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/54.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/54.jpg"
  },
  "nat": "FR"
}, {
  "gender": "female",
  "name": {
    "title": "Miss",
    "first": "Emily",
    "last": "Harris"
  },
  "location": {
    "street": {
      "number": 1184,
      "name": "Target Road"
    },
    "city": "Napier",
    "state": "Gisborne",
    "country": "New Zealand",
    "postcode": 53319,
    "coordinates": {
      "latitude": "-60.6610",
      "longitude": "58.4030"
    },
    "timezone": {
      "offset": "0:00",
      "description": "Western Europe Time, London, Lisbon, Casablanca"
    }
  },
  "email": "emily.harris@example.com",
  "login": {
    "uuid": "dcca6e72-3e59-4504-9fa3-88f500d300a9",
    "username": "beautifulbutterfly419",
    "password": "joker1",
    "salt": "3MSRwVih",
    "md5": "b8f0f73c35c813901369478f47593285",
    "sha1": "33b06a71155fa64fe42181ca5452ea168b248ff7",
    "sha256": "a0c1c1a96397608ca983e22aab64d11cf21a3bcb624bb31dd9224fbe425ad1cb"
  },
  "dob": {
    "date": "1973-05-12T15:56:32.361Z",
    "age": 49
  },
  "registered": {
    "date": "2010-08-21T02:24:23.256Z",
    "age": 12
  },
  "phone": "(210)-587-4060",
  "cell": "(089)-346-1343",
  "id": {
    "name": "",
    "value": null
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/26.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/26.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/26.jpg"
  },
  "nat": "NZ"
}, {
  "gender": "male",
  "name": {
    "title": "Mr",
    "first": "Pedro",
    "last": "Alvarez"
  },
  "location": {
    "street": {
      "number": 1579,
      "name": "Calle Covadonga"
    },
    "city": "Granada",
    "state": "Comunidad Valenciana",
    "country": "Spain",
    "postcode": 18728,
    "coordinates": {
      "latitude": "-12.3219",
      "longitude": "166.8279"
    },
    "timezone": {
      "offset": "+4:00",
      "description": "Abu Dhabi, Muscat, Baku, Tbilisi"
    }
  },
  "email": "pedro.alvarez@example.com",
  "login": {
    "uuid": "9610cb82-5c66-4125-bb8c-1c2fd9ec8bf5",
    "username": "crazycat429",
    "password": "deeper",
    "salt": "TZDg25Eh",
    "md5": "dd6b961ded903582bcc7fa09dae9e700",
    "sha1": "a82f2642719f472e3fd8c3ac4cca2b06ff08292c",
    "sha256": "32ede424ce2d955094035da4b82457e057ee33ade57b024c76b7004ce9285531"
  },
  "dob": {
    "date": "1977-02-07T19:02:38.615Z",
    "age": 45
  },
  "registered": {
    "date": "2017-05-14T02:33:57.657Z",
    "age": 5
  },
  "phone": "985-863-974",
  "cell": "680-490-170",
  "id": {
    "name": "DNI",
    "value": "08444281-V"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/men/14.jpg",
    "medium": "https://randomuser.me/api/portraits/med/men/14.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/men/14.jpg"
  },
  "nat": "ES"
}, {
  "gender": "female",
  "name": {
    "title": "Mrs",
    "first": "Joya",
    "last": "Blaauboer"
  },
  "location": {
    "street": {
      "number": 4802,
      "name": "Heysekade"
    },
    "city": "Terlinden",
    "state": "Noord-Holland",
    "country": "Netherlands",
    "postcode": 31563,
    "coordinates": {
      "latitude": "9.8364",
      "longitude": "172.5798"
    },
    "timezone": {
      "offset": "+1:00",
      "description": "Brussels, Copenhagen, Madrid, Paris"
    }
  },
  "email": "joya.blaauboer@example.com",
  "login": {
    "uuid": "3c551ae1-b9e6-4aec-824a-3ba358a6e283",
    "username": "angryladybug929",
    "password": "zzzzzzzz",
    "salt": "KUSmgAPr",
    "md5": "7c6cada4710b99510fd6309c4c99e4bb",
    "sha1": "fc526b5dceb2427f210bfc90cdd55cbaab1934fd",
    "sha256": "a8b3bb3de2af2e6cd16e504e9645a02bb603b3b4542b2866e809fdae5f491163"
  },
  "dob": {
    "date": "1963-03-24T09:23:00.251Z",
    "age": 59
  },
  "registered": {
    "date": "2009-07-15T18:21:29.389Z",
    "age": 13
  },
  "phone": "(512)-165-5248",
  "cell": "(645)-641-7147",
  "id": {
    "name": "BSN",
    "value": "10823678"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/31.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/31.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/31.jpg"
  },
  "nat": "NL"
}, {
  "gender": "female",
  "name": {
    "title": "Mrs",
    "first": "Monica",
    "last": "Iglesias"
  },
  "location": {
    "street": {
      "number": 771,
      "name": "Paseo de Zorrilla"
    },
    "city": "Alcobendas",
    "state": "País Vasco",
    "country": "Spain",
    "postcode": 71733,
    "coordinates": {
      "latitude": "-0.5774",
      "longitude": "38.0307"
    },
    "timezone": {
      "offset": "+5:45",
      "description": "Kathmandu"
    }
  },
  "email": "monica.iglesias@example.com",
  "login": {
    "uuid": "709768d1-7de2-476d-b57b-18f431fbce1a",
    "username": "blackdog775",
    "password": "worm",
    "salt": "z9yFPrV6",
    "md5": "5c64b5899ab752b57b2c99b50bc3cda8",
    "sha1": "d5ed8e280789723efbc61546bf611f92809e0295",
    "sha256": "a2eb5d1f756649cc5cbccb3bf6a0d939620ec9847147db0cd1b8ac6bd3d28a90"
  },
  "dob": {
    "date": "1989-03-29T12:32:01.702Z",
    "age": 33
  },
  "registered": {
    "date": "2008-04-09T20:53:46.514Z",
    "age": 14
  },
  "phone": "956-269-776",
  "cell": "681-485-932",
  "id": {
    "name": "DNI",
    "value": "23229288-U"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/63.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/63.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/63.jpg"
  },
  "nat": "ES"
}, {
  "gender": "male",
  "name": {
    "title": "Mr",
    "first": "Mads",
    "last": "Rasmussen"
  },
  "location": {
    "street": {
      "number": 5816,
      "name": "Skolegade"
    },
    "city": "Tisvilde",
    "state": "Syddanmark",
    "country": "Denmark",
    "postcode": 72757,
    "coordinates": {
      "latitude": "3.3211",
      "longitude": "-94.1170"
    },
    "timezone": {
      "offset": "+5:30",
      "description": "Bombay, Calcutta, Madras, New Delhi"
    }
  },
  "email": "mads.rasmussen@example.com",
  "login": {
    "uuid": "108866db-29a9-4882-8c20-696bb3b1c56c",
    "username": "lazydog810",
    "password": "pictuers",
    "salt": "8zu9iUtM",
    "md5": "035da6afb9ee522358c9c97186e1955a",
    "sha1": "8129b1dcb79b1a7c595dadbfd4798964bff7a7fe",
    "sha256": "a084057e2177d58e171d49dff475200a778e942c0dde0346164ef62bd14ad1be"
  },
  "dob": {
    "date": "1949-08-08T13:20:37.746Z",
    "age": 73
  },
  "registered": {
    "date": "2014-04-19T16:59:46.925Z",
    "age": 8
  },
  "phone": "85993431",
  "cell": "67766075",
  "id": {
    "name": "CPR",
    "value": "080849-4617"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/men/12.jpg",
    "medium": "https://randomuser.me/api/portraits/med/men/12.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/men/12.jpg"
  },
  "nat": "DK"
}, {
  "gender": "female",
  "name": {
    "title": "Miss",
    "first": "رونیکا",
    "last": "سلطانی نژاد"
  },
  "location": {
    "street": {
      "number": 4851,
      "name": "کوی نصر"
    },
    "city": "خمینی‌شهر",
    "state": "مرکزی",
    "country": "Iran",
    "postcode": 96539,
    "coordinates": {
      "latitude": "12.6359",
      "longitude": "-3.9061"
    },
    "timezone": {
      "offset": "-6:00",
      "description": "Central Time (US & Canada), Mexico City"
    }
  },
  "email": "rwnykh.sltnynjd@example.com",
  "login": {
    "uuid": "668ae843-171d-4737-aabb-5c2171158744",
    "username": "heavywolf830",
    "password": "atomic",
    "salt": "4vjUTUeI",
    "md5": "d52a9ea7e78a5801533cdd3d4b3bd886",
    "sha1": "0a4c343c084475b4b7529eb9cf89ace90e0398b5",
    "sha256": "6bf545cf0fd8f90af4399809b399899bc9e95fd37f8553dbf37f3bc456956c10"
  },
  "dob": {
    "date": "1966-03-29T17:05:10.874Z",
    "age": 56
  },
  "registered": {
    "date": "2014-01-10T20:31:46.188Z",
    "age": 8
  },
  "phone": "043-09256368",
  "cell": "0971-609-1544",
  "id": {
    "name": "",
    "value": null
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/76.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/76.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/76.jpg"
  },
  "nat": "IR"
}, {
  "gender": "male",
  "name": {
    "title": "Mr",
    "first": "Dennis",
    "last": "Mason"
  },
  "location": {
    "street": {
      "number": 4701,
      "name": "The Green"
    },
    "city": "Gorey",
    "state": "Laois",
    "country": "Ireland",
    "postcode": 37898,
    "coordinates": {
      "latitude": "-71.6701",
      "longitude": "-158.7608"
    },
    "timezone": {
      "offset": "-4:00",
      "description": "Atlantic Time (Canada), Caracas, La Paz"
    }
  },
  "email": "dennis.mason@example.com",
  "login": {
    "uuid": "8eb70a11-df56-4a25-b553-d0923581bd71",
    "username": "silvermouse742",
    "password": "wxcvbn",
    "salt": "2Ovu1aLd",
    "md5": "5ccf57e45df09122a83bd7acbe7933b9",
    "sha1": "4e68f109a0eda4ccca88c70add7bde3cd416d319",
    "sha256": "b7cf47cb11a346b3062c2a3497652ddc24ebf4c83fdf416855379b77cdf076a9"
  },
  "dob": {
    "date": "1987-08-27T06:19:53.493Z",
    "age": 35
  },
  "registered": {
    "date": "2017-09-22T18:11:04.355Z",
    "age": 5
  },
  "phone": "041-768-9121",
  "cell": "081-084-3655",
  "id": {
    "name": "PPS",
    "value": "1913271T"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/men/60.jpg",
    "medium": "https://randomuser.me/api/portraits/med/men/60.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/men/60.jpg"
  },
  "nat": "IE"
}, {
  "gender": "female",
  "name": {
    "title": "Ms",
    "first": "Kornelia",
    "last": "Sagstad"
  },
  "location": {
    "street": {
      "number": 4590,
      "name": "Alundamveien"
    },
    "city": "Jortveit",
    "state": "Telemark",
    "country": "Norway",
    "postcode": "5398",
    "coordinates": {
      "latitude": "65.3446",
      "longitude": "145.2843"
    },
    "timezone": {
      "offset": "+4:30",
      "description": "Kabul"
    }
  },
  "email": "kornelia.sagstad@example.com",
  "login": {
    "uuid": "7144259d-2e9c-4b5f-87e0-7e11c11d6764",
    "username": "heavymeercat643",
    "password": "frosty",
    "salt": "5HT2QeZR",
    "md5": "4c44a01cc1e44962e3e0d46a8d701e42",
    "sha1": "8d914964563230547731c926fa300fb8094fd56d",
    "sha256": "0012dbbae42c233eaf2ebb8a16d6c8841b04ebcaced5d6052a91f7558a18c6d2"
  },
  "dob": {
    "date": "1979-01-29T18:24:05.264Z",
    "age": 43
  },
  "registered": {
    "date": "2018-07-12T21:53:41.451Z",
    "age": 4
  },
  "phone": "30952186",
  "cell": "48515386",
  "id": {
    "name": "FN",
    "value": "29017914861"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/20.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/20.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/20.jpg"
  },
  "nat": "NO"
}, {
  "gender": "male",
  "name": {
    "title": "Mr",
    "first": "Ahmet",
    "last": "Dalkıran"
  },
  "location": {
    "street": {
      "number": 6720,
      "name": "Tunalı Hilmi Cd"
    },
    "city": "Artvin",
    "state": "Muş",
    "country": "Turkey",
    "postcode": 56866,
    "coordinates": {
      "latitude": "56.7534",
      "longitude": "-110.8362"
    },
    "timezone": {
      "offset": "-3:30",
      "description": "Newfoundland"
    }
  },
  "email": "ahmet.dalkiran@example.com",
  "login": {
    "uuid": "dbd70777-bd93-49bb-b16a-4f07e5659243",
    "username": "beautifulkoala868",
    "password": "death",
    "salt": "mhQysXFK",
    "md5": "0df4c7d5d67452ae1f172ddb1215e024",
    "sha1": "b2d5be93238407f3974d40ae893a7bb0af4363f5",
    "sha256": "0d39d3ff463a77bafc9a0fcd9cdd48c1b072c0ecf59ab798a6d0d0a2e452c332"
  },
  "dob": {
    "date": "1983-09-24T00:20:53.374Z",
    "age": 39
  },
  "registered": {
    "date": "2010-03-02T09:00:32.346Z",
    "age": 12
  },
  "phone": "(137)-889-7726",
  "cell": "(155)-060-1585",
  "id": {
    "name": "",
    "value": null
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/men/37.jpg",
    "medium": "https://randomuser.me/api/portraits/med/men/37.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/men/37.jpg"
  },
  "nat": "TR"
}, {
  "gender": "male",
  "name": {
    "title": "Mr",
    "first": "Yann",
    "last": "Gonzalez"
  },
  "location": {
    "street": {
      "number": 7544,
      "name": "Rue du Bât-D'Argent"
    },
    "city": "Rennes",
    "state": "Mayenne",
    "country": "France",
    "postcode": 97559,
    "coordinates": {
      "latitude": "29.4329",
      "longitude": "14.0255"
    },
    "timezone": {
      "offset": "+4:30",
      "description": "Kabul"
    }
  },
  "email": "yann.gonzalez@example.com",
  "login": {
    "uuid": "846d979a-61c3-47c2-8d35-9bd6fa78c1f0",
    "username": "lazytiger625",
    "password": "logger",
    "salt": "yvwpV2pu",
    "md5": "e8dcc805a52d8c6178c5a374f8da6d23",
    "sha1": "025d84307597342bf93fea96ef4c13bf4f956a0a",
    "sha256": "031a90b00a5c254dea3268257b003ba0c5fdb8950d38a1f6e798cbef8d52697c"
  },
  "dob": {
    "date": "1950-10-12T19:10:49.489Z",
    "age": 72
  },
  "registered": {
    "date": "2012-01-09T23:25:20.040Z",
    "age": 10
  },
  "phone": "03-41-87-68-29",
  "cell": "06-46-90-99-20",
  "id": {
    "name": "INSEE",
    "value": "1NNaN32653445 33"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/men/37.jpg",
    "medium": "https://randomuser.me/api/portraits/med/men/37.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/men/37.jpg"
  },
  "nat": "FR"
}, {
  "gender": "male",
  "name": {
    "title": "Mr",
    "first": "Richano",
    "last": "Schuite"
  },
  "location": {
    "street": {
      "number": 1943,
      "name": "Hoevelakenstraat"
    },
    "city": "'s-Heer Abtskerke",
    "state": "Limburg",
    "country": "Netherlands",
    "postcode": 83613,
    "coordinates": {
      "latitude": "71.3155",
      "longitude": "66.9338"
    },
    "timezone": {
      "offset": "+2:00",
      "description": "Kaliningrad, South Africa"
    }
  },
  "email": "richano.schuite@example.com",
  "login": {
    "uuid": "6f68a4ce-f22c-4d5a-8b3a-fbbae27d8f64",
    "username": "bluekoala103",
    "password": "melanie",
    "salt": "CbdUaYGm",
    "md5": "9e1acbcf8231b322098b59d43a35b324",
    "sha1": "9236c35513de4ee624ef0d1543540478201d3058",
    "sha256": "80edec54a3e4fe69585da782bb45fb1e5b81f254d4d1ddd55d7723d42a46f2fc"
  },
  "dob": {
    "date": "1994-08-22T23:30:01.655Z",
    "age": 28
  },
  "registered": {
    "date": "2019-03-29T02:49:22.392Z",
    "age": 3
  },
  "phone": "(464)-228-0641",
  "cell": "(552)-679-7492",
  "id": {
    "name": "BSN",
    "value": "98321491"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/men/91.jpg",
    "medium": "https://randomuser.me/api/portraits/med/men/91.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/men/91.jpg"
  },
  "nat": "NL"
}, {
  "gender": "female",
  "name": {
    "title": "Mrs",
    "first": "Isabelle",
    "last": "Thomas"
  },
  "location": {
    "street": {
      "number": 6549,
      "name": "South Eastern Highway"
    },
    "city": "Dunedin",
    "state": "Auckland",
    "country": "New Zealand",
    "postcode": 11284,
    "coordinates": {
      "latitude": "-40.3879",
      "longitude": "-65.1933"
    },
    "timezone": {
      "offset": "-2:00",
      "description": "Mid-Atlantic"
    }
  },
  "email": "isabelle.thomas@example.com",
  "login": {
    "uuid": "43e4dec5-48db-49be-9bf6-5f2b4ab72c1b",
    "username": "ticklishswan817",
    "password": "aviation",
    "salt": "B8M7q1mW",
    "md5": "ff51902d04c8ccafdee48513d16a3cd1",
    "sha1": "f71ba0edd56ef07438492c41c3da6054a228400f",
    "sha256": "13f25a6c1067e635f2164e7948d1c33005f5c45a3080294e018f5af2763ee1ae"
  },
  "dob": {
    "date": "1968-12-03T07:12:45.090Z",
    "age": 54
  },
  "registered": {
    "date": "2014-03-09T15:39:26.266Z",
    "age": 8
  },
  "phone": "(118)-744-3853",
  "cell": "(201)-132-6032",
  "id": {
    "name": "",
    "value": null
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/74.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/74.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/74.jpg"
  },
  "nat": "NZ"
}, {
  "gender": "female",
  "name": {
    "title": "Mrs",
    "first": "Nerci",
    "last": "Ramos"
  },
  "location": {
    "street": {
      "number": 8818,
      "name": "Rua Treze "
    },
    "city": "Patos de Minas",
    "state": "Maranhão",
    "country": "Brazil",
    "postcode": 85086,
    "coordinates": {
      "latitude": "-66.8524",
      "longitude": "123.1108"
    },
    "timezone": {
      "offset": "+4:00",
      "description": "Abu Dhabi, Muscat, Baku, Tbilisi"
    }
  },
  "email": "nerci.ramos@example.com",
  "login": {
    "uuid": "9ff5752a-bcaa-41c9-ba1c-ae65337f5afe",
    "username": "angrybutterfly307",
    "password": "marc",
    "salt": "hdtz0bD8",
    "md5": "e3bddbdf5acdcd9ea16f57a58613e381",
    "sha1": "c0c0e0b7ed0375c9fd1b0773c398a1de09ae22b3",
    "sha256": "d1a9e7cdc9402a0e3a2fe4e809f93420b1e7c4ffdc3fa1e39970e4b055c9a433"
  },
  "dob": {
    "date": "1965-07-28T18:53:17.442Z",
    "age": 57
  },
  "registered": {
    "date": "2011-12-18T16:24:59.347Z",
    "age": 11
  },
  "phone": "(10) 1115-5876",
  "cell": "(77) 4711-2398",
  "id": {
    "name": "",
    "value": null
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/24.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/24.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/24.jpg"
  },
  "nat": "BR"
}, {
  "gender": "male",
  "name": {
    "title": "Monsieur",
    "first": "Julius",
    "last": "Gonzalez"
  },
  "location": {
    "street": {
      "number": 105,
      "name": "Avenue de la République"
    },
    "city": "Lohn-Ammannsegg",
    "state": "Jura",
    "country": "Switzerland",
    "postcode": 7558,
    "coordinates": {
      "latitude": "-28.6277",
      "longitude": "-125.7870"
    },
    "timezone": {
      "offset": "-3:30",
      "description": "Newfoundland"
    }
  },
  "email": "julius.gonzalez@example.com",
  "login": {
    "uuid": "bbb1817f-c6ac-4760-b39a-5acb190e2967",
    "username": "ticklishcat788",
    "password": "maria",
    "salt": "QbHvmwL9",
    "md5": "c1ebda07679c40de500fd29fd4b299bb",
    "sha1": "29cc37a853478c3933abc08f562ce45529d7b877",
    "sha256": "8d461e9381f18afdb903551304623705fd2f5ac6aae4aeb5a3c1f0278c55c7b9"
  },
  "dob": {
    "date": "1945-08-03T11:34:14.077Z",
    "age": 77
  },
  "registered": {
    "date": "2007-02-22T04:55:25.558Z",
    "age": 15
  },
  "phone": "075 396 22 03",
  "cell": "078 467 45 16",
  "id": {
    "name": "AVS",
    "value": "756.8256.5174.28"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/men/42.jpg",
    "medium": "https://randomuser.me/api/portraits/med/men/42.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/men/42.jpg"
  },
  "nat": "CH"
}, {
  "gender": "female",
  "name": {
    "title": "Mrs",
    "first": "فاطمه",
    "last": "حسینی"
  },
  "location": {
    "street": {
      "number": 7581,
      "name": "آیت الله مدرس"
    },
    "city": "نیشابور",
    "state": "ایلام",
    "country": "Iran",
    "postcode": 15637,
    "coordinates": {
      "latitude": "24.7556",
      "longitude": "-19.5248"
    },
    "timezone": {
      "offset": "+7:00",
      "description": "Bangkok, Hanoi, Jakarta"
    }
  },
  "email": "ftmh.hsyny@example.com",
  "login": {
    "uuid": "3947e674-da55-4d09-8fca-bce97cbcf231",
    "username": "ticklishfrog791",
    "password": "anubis",
    "salt": "njXBKHMd",
    "md5": "9ebe9d6464ae2138a737133264444419",
    "sha1": "328e4b2842e1ddd88b880f0d1e1e83ef052bc637",
    "sha256": "bd12232136b75c3323f703b5d303a7e6ff7b775e5a2b5b53bb63a475f508456c"
  },
  "dob": {
    "date": "1996-01-17T15:21:22.794Z",
    "age": 26
  },
  "registered": {
    "date": "2009-04-16T10:49:17.271Z",
    "age": 13
  },
  "phone": "052-65328430",
  "cell": "0972-082-1071",
  "id": {
    "name": "",
    "value": null
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/78.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/78.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/78.jpg"
  },
  "nat": "IR"
}, {
  "gender": "male",
  "name": {
    "title": "Mr",
    "first": "آرتين",
    "last": "سهيلي راد"
  },
  "location": {
    "street": {
      "number": 1784,
      "name": "پارک طالقانی"
    },
    "city": "اردبیل",
    "state": "گلستان",
    "country": "Iran",
    "postcode": 56615,
    "coordinates": {
      "latitude": "-73.8173",
      "longitude": "123.8580"
    },
    "timezone": {
      "offset": "+5:30",
      "description": "Bombay, Calcutta, Madras, New Delhi"
    }
  },
  "email": "artyn.shylyrd@example.com",
  "login": {
    "uuid": "75a982f1-54c2-4d98-bda7-05cae8bda1db",
    "username": "tinywolf518",
    "password": "farside",
    "salt": "5yx2rM4U",
    "md5": "80e21ae2d1ce36b750662f86ea80f53f",
    "sha1": "04c4d85deb6e50b36df768952d1fd731a550bc5d",
    "sha256": "8cafb12439a3a8ad44ff0e0ffe358b292867c7e777d4bcf7a5f118f65023c957"
  },
  "dob": {
    "date": "1972-06-03T22:52:14.668Z",
    "age": 50
  },
  "registered": {
    "date": "2007-05-22T03:25:23.326Z",
    "age": 15
  },
  "phone": "079-78449393",
  "cell": "0985-525-4368",
  "id": {
    "name": "",
    "value": null
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/men/37.jpg",
    "medium": "https://randomuser.me/api/portraits/med/men/37.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/men/37.jpg"
  },
  "nat": "IR"
}, {
  "gender": "male",
  "name": {
    "title": "Mr",
    "first": "Arttu",
    "last": "Saksa"
  },
  "location": {
    "street": {
      "number": 2397,
      "name": "Hermiankatu"
    },
    "city": "Konnevesi",
    "state": "Lapland",
    "country": "Finland",
    "postcode": 53682,
    "coordinates": {
      "latitude": "-22.7292",
      "longitude": "-10.4955"
    },
    "timezone": {
      "offset": "0:00",
      "description": "Western Europe Time, London, Lisbon, Casablanca"
    }
  },
  "email": "arttu.saksa@example.com",
  "login": {
    "uuid": "99ae321f-843d-4a0e-9186-dab9e5044aa1",
    "username": "brownmouse771",
    "password": "ginscoot",
    "salt": "NwRFiZn6",
    "md5": "cadc44317d5aee197e131be2987d617a",
    "sha1": "7dfd28ef37f36244f92c4592cb9752d268eed23a",
    "sha256": "d6e54fa24241749fe6449aba9585929e0dc840c6da3e5abe7a077367b590b06b"
  },
  "dob": {
    "date": "1989-08-27T03:30:07.806Z",
    "age": 33
  },
  "registered": {
    "date": "2007-02-11T17:26:49.419Z",
    "age": 15
  },
  "phone": "06-936-861",
  "cell": "044-897-41-29",
  "id": {
    "name": "HETU",
    "value": "NaNNA887undefined"
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/men/86.jpg",
    "medium": "https://randomuser.me/api/portraits/med/men/86.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/men/86.jpg"
  },
  "nat": "FI"
}, {
  "gender": "female",
  "name": {
    "title": "Miss",
    "first": "Eleutéria",
    "last": "Silveira"
  },
  "location": {
    "street": {
      "number": 1634,
      "name": "Rua Paraná "
    },
    "city": "Codó",
    "state": "Santa Catarina",
    "country": "Brazil",
    "postcode": 38888,
    "coordinates": {
      "latitude": "20.1321",
      "longitude": "7.4666"
    },
    "timezone": {
      "offset": "-9:00",
      "description": "Alaska"
    }
  },
  "email": "eleuteria.silveira@example.com",
  "login": {
    "uuid": "31a64c89-417d-4b76-a5ce-debc3a1981a0",
    "username": "heavyleopard990",
    "password": "microsoft",
    "salt": "pcnJmoRE",
    "md5": "db203840032c0581a1d29f60ac3ffe2e",
    "sha1": "a45daa2198c5a606abe41310b1cb875f976ad02d",
    "sha256": "56229067c3e8533f1bac042c39a67a6a079ec3ff9e108a31669510877c9d6899"
  },
  "dob": {
    "date": "1957-03-22T19:42:20.762Z",
    "age": 65
  },
  "registered": {
    "date": "2003-04-11T00:17:06.248Z",
    "age": 19
  },
  "phone": "(07) 2213-1126",
  "cell": "(86) 6591-1211",
  "id": {
    "name": "",
    "value": null
  },
  "picture": {
    "large": "https://randomuser.me/api/portraits/women/1.jpg",
    "medium": "https://randomuser.me/api/portraits/med/women/1.jpg",
    "thumbnail": "https://randomuser.me/api/portraits/thumb/women/1.jpg"
  },
  "nat": "BR"
}]

test('simulate search first name with keyword', () => {
  const keyword = 'minea';
  const result = searchData(data, keyword)
  const expected = [{"gender":"female","name":{"title":"Ms","first":"Minea","last":"Lehtinen"},"location":{"street":{"number":9047,"name":"Visiokatu"},"city":"Kitee","state":"Kainuu","country":"Finland","postcode":80866,"coordinates":{"latitude":"6.2393","longitude":"-98.8400"},"timezone":{"offset":"+9:00","description":"Tokyo, Seoul, Osaka, Sapporo, Yakutsk"}},"email":"minea.lehtinen@example.com","login":{"uuid":"e84620a3-b789-47ee-af52-c1dce632dcd2","username":"smalldog531","password":"sidekick","salt":"fZX4xnlZ","md5":"47f33c494af6d8727dcc452657f6f967","sha1":"c2c47f76626c4e061bcbaf3c48413c4cbee164d1","sha256":"13c3d19b57f42129631869aaddcd0b15ed769ae1f32adbc9a16e0b7076478135"},"dob":{"date":"1987-01-17T13:05:47.580Z","age":35},"registered":{"date":"2016-01-08T17:13:26.628Z","age":6},"phone":"09-970-322","cell":"041-017-98-78","id":{"name":"HETU","value":"NaNNA930undefined"},"picture":{"large":"https://randomuser.me/api/portraits/women/75.jpg","medium":"https://randomuser.me/api/portraits/med/women/75.jpg","thumbnail":"https://randomuser.me/api/portraits/thumb/women/75.jpg"},"nat":"FI"}]
  
  expect(result).toMatchObject(expected)
});
