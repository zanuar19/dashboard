export const PAGE_SIZE = 5;
export const STORAGE_NAME = 'api-storage';
export const API_HOST = process.env.REACT_APP_API;

export enum SCREEN_SIZE {
  mobile = '600px'
}

export enum STATUS_FETCH {
  IDLE = 'idle',
  PENDING = 'pending',
  FAILED = 'failed',
  RESOLVED = 'resolved'
}
