import { PAGE_SIZE, STORAGE_NAME } from 'helpers/constants';

export const searchData = (array: Array<{}>, keyword: string) => {
  return array.filter((s: any) => {
    return s.name.first.toLowerCase().includes(keyword.toLowerCase());
  });
};

export const paginate = (array: any, page_number: any) => {
  return array.slice((page_number - 1) * PAGE_SIZE, page_number * PAGE_SIZE);
};

export const getStorage = () => {
	return JSON.parse(window.localStorage.getItem(STORAGE_NAME) as any) || [];
};

export const setStorage = (data: any) => {
	return window.localStorage.setItem(STORAGE_NAME, JSON.stringify(data));
};
