import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders app component show text driver management', () => {
  render(<App />);
  const element = screen.getByText(/driver management/i);
  expect(element).toBeInTheDocument();
});
