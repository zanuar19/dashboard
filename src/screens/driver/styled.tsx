import styled from '@emotion/styled';
import { SCREEN_SIZE } from 'helpers/constants';

export const Content = styled.div`
  display: grid;
  grid-template-columns: 1fr;
`;

export const Wrapper = styled.div`
  padding: 10px 25px;
  display: flex;
  flex-wrap: nowrap;
  gap: 20px;
  overflow-x: auto;
  &::-webkit-scrollbar {
    display: none;
  }
  @media (max-width: ${SCREEN_SIZE.mobile}) {
    flex-direction: column;
  }
`;
