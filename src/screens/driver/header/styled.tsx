import styled from '@emotion/styled';
import { SCREEN_SIZE } from 'helpers/constants';

type HeadingProps = {
  title?: string;
  description?: string;
};

type SearchProps = {
  icon?: React.ReactNode;
  children?: React.ReactNode;
};

export const Wrapper = styled.div`
  padding: 25px;
`;

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 16px 25px;
  background: #fff;
  @media (max-width: ${SCREEN_SIZE.mobile}) {
    flex-direction: column;
    padding: 12px 20px;
    justify-content: center;
    align-items: flex-start;
  }
`;

export const Heading = styled.div`
  //   background: red;
`;

const Title = styled.div`
  text-transform: uppercase;
  font-weight: 800;
  font-size: 20px;
  color: #ff4646;
`;

const Description = styled.div`
  font-weight: 400;
  font-size: 13px;
`;

export const HeadingNew = styled(({ title, description }: HeadingProps) => (
  <div>
    <Title>{title}</Title>
    <Description>{description}</Description>
  </div>
))<HeadingProps>``;

export const ActionTool = styled.div`
  display: flex;
  gap: 10px;
  @media (max-width: ${SCREEN_SIZE.mobile}) {
    flex-direction: column;
    justify-content: center;
    margin-top: 15px;
    width: 100%;
  }
`;

export const IconWrapper = styled.div`
  position: absolute;
  margin-left: 6px;
  color: #ff4646;
  font-size: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const SearchWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: relative;
  font-family: Montserrat;
`;

export const Search = styled(({ icon, children }: SearchProps) => (
  <SearchWrapper>
    {icon && <IconWrapper>{icon}</IconWrapper>}
    {children}
  </SearchWrapper>
))<SearchProps>``;

export const TextInput = styled.input`
  background-color: transparent;
  border: 2px solid #e4e6ef;
  padding: 8px 8px 8px 30px;
  width: 130px;
  &:focus {
    outline: none;
  }
  &:hover,
  &:active {
    color: #ff4646;
    background: #fff;
    border: 2px solid #ff4646;
  }
  @media (max-width: ${SCREEN_SIZE.mobile}) {
    width: 100%;
  }
`;

export const Button = styled.button`
  border: none;
  background: #ff4646;
  color: #fff;
  text-transform: uppercase;
  font-family: Montserrat;
  font-size: 10px;
  font-weight: 800;
`;
