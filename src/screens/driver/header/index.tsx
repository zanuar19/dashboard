import React from 'react';
import {
  Wrapper,
  Container,
  HeadingNew,
  ActionTool,
  TextInput,
  Search
} from './styled';
import { BiSearch } from 'react-icons/bi';
import { FiPlus } from 'react-icons/fi';
import ButtonIcon from 'components/ButtonIcon';
import { useAppDispatch } from 'reduxState/hooks';
import { setSearch } from 'reduxState/reducers/driverSlice';

const Header = () => {
  const dispatch = useAppDispatch();

  return (
    <Wrapper>
      <Container>
        <HeadingNew
          title="Driver Management"
          description="Data driver yang bekerja dengan anda"
        />
        <ActionTool>
          <Search icon={<BiSearch />}>
            <TextInput
              placeholder="Cari Driver"
              onChange={(e) => dispatch(setSearch(e.target.value))}
            />
          </Search>
          <ButtonIcon icon={<FiPlus />}>Tambah Driver</ButtonIcon>
        </ActionTool>
      </Container>
    </Wrapper>
  );
};

export default Header;
