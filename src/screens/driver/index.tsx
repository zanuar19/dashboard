import React, { useCallback, useEffect, memo } from 'react';
import { Content, Wrapper } from './styled';
import Card from 'components/Card';
import Pagination from 'components/Pagination';
import { useAppDispatch, useAppSelector } from 'reduxState/hooks';
import {
  DriverData,
  fetchDriver,
  selectDriver,
  selectPage,
  selectTotalPage,
  setPage
} from 'reduxState/reducers/driverSlice';
import { STATUS_FETCH } from 'helpers/constants';
import Alert from 'components/Alert';
import Header from 'screens/driver/Header';

const Driver = () => {
  const dataDriver = useAppSelector(selectDriver);
  const page = useAppSelector(selectPage);
  const totalPage = useAppSelector(selectTotalPage);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchDriver());
  }, [dispatch]);

  const handlePrev = useCallback(() => {
    dispatch(setPage(page - 1));
  }, [dispatch, page]);

  const handleNext = useCallback(() => {
    dispatch(setPage(page + 1));
  }, [dispatch, page]);

  return (
    <Content>
      <Header />
      {dataDriver.status === STATUS_FETCH.RESOLVED &&
      !dataDriver.data.length ? (
        <Alert text="Not Found" />
      ) : (
        <></>
      )}
      <Wrapper>
        {dataDriver.status === STATUS_FETCH.RESOLVED &&
          dataDriver.data.map((driver: DriverData) => (
            <Card
              key={driver.login.uuid}
              driverId={driver.login.username}
              picture={driver.picture.medium}
              name={`${driver.name.first}, ${driver.name.last}`}
              phone={driver.phone}
              email={driver.email}
              dob={driver.dob.date}
            />
          ))}
      </Wrapper>
      {dataDriver.status === STATUS_FETCH.RESOLVED && dataDriver.data.length ? (
        <Pagination
          onPrev={handlePrev}
          onNext={handleNext}
          isDisabledPrev={page <= 1}
          isDisabledNext={page === totalPage}
        />
      ) : (
        <></>
      )}
    </Content>
  );
};

export default memo(Driver);
