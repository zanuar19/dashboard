import styled from '@emotion/styled';
import { SCREEN_SIZE } from 'helpers/constants';

type ButtonProps = {
  icon?: React.ReactNode;
  children?: React.ReactNode;
  onClick?: React.MouseEventHandler<HTMLElement>;
};

const ButtonWrapper = styled.button`
  border: none;
  background: #ff4646;
  border: 2px solid #ff4646;
  color: #fff;
  text-transform: uppercase;
  font-family: Montserrat;
  font-size: 10px;
  font-weight: 800;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 8px;
  cursor: pointer;
  &:hover {
    color: #ff4646;
    background: #fff;
  }
  &:active {
    color: #ff4646;
    background: #d5d4d4;
  }
  @media (max-width: ${SCREEN_SIZE.mobile}) {
    font-size: 12px;
    padding: 6px;
    justify-content: flex-start;
  }
`;

const IconWrapper = styled.div`
  font-size: 20px;
  display: flex;
  align-items: center;
`;

export const Button = styled(({ icon, children, ...props }: ButtonProps) => (
  <ButtonWrapper aria-label="button" {...props}>
    {children}
    {icon && <IconWrapper>{icon}</IconWrapper>}
  </ButtonWrapper>
))<ButtonProps>``;
