import React from 'react';
import { Button } from './styled';

interface props {
  icon?: React.ReactNode;
  children?: React.ReactNode;
}

const ButtonIcon = ({ icon, children }: props) => {
  return <Button icon={icon}>{children}</Button>;
};

export default ButtonIcon;
