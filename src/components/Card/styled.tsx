import styled from '@emotion/styled';
import { SCREEN_SIZE } from 'helpers/constants';

export const Wrapper = styled.div`
  width: 270px;
  background: #fff;
  border-radius: 3px;
  overflow: hidden;
  flex: 0 0 auto;
  border: 1px solid #fff;
  min-height: 400px;
  &:hover {
    border: 1px solid #ff4646;
  }
  @media (max-width: ${SCREEN_SIZE.mobile}) {
    width: 100%;
    min-height: auto;
  }
`;

export const Head = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px 20px;
  border-bottom: 2px solid #aaaaaa;
  color: #aaaaaa;
  font-size: 12px;
  font-weight: 600;
  & > span > span {
    color: #ff4646;
    font-weight: 600;
  }
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 20px;
  @media (max-width: ${SCREEN_SIZE.mobile}) {
    flex-direction: row;
    gap: 20px;
  }
`;

export const Image = styled.img`
  width: 100px;
  height: 100px;
  margin: 20px 0;
  border-radius: 50%;
  @media (max-width: ${SCREEN_SIZE.mobile}) {
    width: 72px;
    height: 72px;
  }
`;

export const InfoList = styled.div`
  display: flex;
  flex-direction: column;
  @media (max-width: ${SCREEN_SIZE.mobile}) {
    justify-content: center;
    aligin-items: center;
    & > div:nth-of-type(n + 3) {
      display: none;
    }
  }
`;
