import React from 'react';
import { Wrapper, Head, Content, Image, InfoList } from './styled';
import { HiOutlineDotsHorizontal } from 'react-icons/hi';
import InfoGroup from 'components/InfoGroup';
import { format } from 'date-fns';

interface Props {
  driverId: string;
  picture: string;
  name: string;
  phone: string;
  email: string;
  dob: Date;
}

const Card = ({ driverId, picture, name, phone, email, dob }: Props) => {
  return (
    <Wrapper>
      <Head>
        <span>
          Driver ID <span>{driverId}</span>
        </span>
        <HiOutlineDotsHorizontal />
      </Head>
      <Content>
        <Image src={picture} alt={name} />
        <InfoList>
          <InfoGroup label="Nama Driver" text={name} />
          <InfoGroup label="Telepon" text={phone} />
          <InfoGroup label="Email" text={email} />
          <InfoGroup
            label="Tanggal Lahir"
            text={format(new Date(dob), 'dd-MM-yyyy')}
          />
        </InfoList>
      </Content>
    </Wrapper>
  );
};

export default Card;
