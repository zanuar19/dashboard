import React from 'react';
import { Wrapper, Loader } from './styled';

interface props {
  children?: string | React.ReactNode;
}

const Loading = ({ children }: props) => {
  return (
    <Wrapper>
      <Loader />
    </Wrapper>
  );
};

export default Loading;
