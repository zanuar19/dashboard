import styled from '@emotion/styled';

export const Wrapper = styled.div`
  position: fixed;
  top: 0;
  height: 100%;
  width: 100%;
  background: rgb(197 197 197 / 5%);
  backdrop-filter: saturate(180%) blur(1px);
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 2;
`;

export const Loader = styled.div`
  border: 16px solid #f3f3f3;
  border-top: 16px solid #3498db;
  border-radius: 50%;
  width: 12px;
  height: 10px;
  margin: 100px auto;
  padding: 20px;
  animation: spin 2s linear infinite;
`;
