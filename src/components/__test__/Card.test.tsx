import React from 'react';
import { render, screen } from '@testing-library/react';
import Card from '../Card';

test('renders card with props', () => {
  render(
    <Card
      driverId="123xxx"
      picture="https://randomuser.me/api/portraits/med/women/3.jpg"
      name="john, doe"
      phone="08123456789"
      email="wkwkwk@mail.com"
      dob={new Date('1969-05-01T05:08:42.784Z')}
    />
  );
  const element = screen.getByText(/john, doe/i);
  expect(element).toBeInTheDocument();
});
