import React from 'react';
import { render as rtlRender, screen } from '@testing-library/react';
import Pagination from '../Pagination';

describe('simulate component pagination', () => {
  const handleClick = jest.fn();
  const view = () => {
    rtlRender(
      <Pagination
        onPrev={handleClick}
        onNext={handleClick}
        isDisabledPrev={true}
        isDisabledNext={false}
      />
    );
  };

  test('success show prev button', () => {
    view();
    const elementPrev = screen.getByRole('button', {
      name: /previous page/i
    });
    expect(elementPrev).toBeInTheDocument();
  });

  test('success show next button', () => {
    view();
    const elementNext = screen.getByRole('button', {
      name: /Next Page/i
    });
    expect(elementNext).toBeInTheDocument();
  });
});
