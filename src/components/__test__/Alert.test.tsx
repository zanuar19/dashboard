import React from 'react';
import { render, screen } from '@testing-library/react';
import Alert from '../Alert';

test('renders alert with text mantap', () => {
  render(<Alert text="mantap" />);
  const alertElement = screen.getByText(/mantap/i);
  expect(alertElement).toBeInTheDocument();
});
