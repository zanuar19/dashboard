import React from 'react';
import { render, screen } from '@testing-library/react';
import ButtonIcon from '../ButtonIcon';
import { FiPlus } from 'react-icons/fi';

test('renders button icon with text Tambah Driver', () => {
  render(<ButtonIcon icon={<FiPlus />}>Tambah Driver</ButtonIcon>);
  const element = screen.getByText(/Tambah Driver/i);
  expect(element).toBeInTheDocument();
});
