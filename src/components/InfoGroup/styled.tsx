import styled from '@emotion/styled';
import { SCREEN_SIZE } from 'helpers/constants';

export const InfoGroup = styled.div`
  display: flex;
  flex-direction: column;
  & label {
    font-size: 12px;
    font-weight: 600;
    color: #aaaaaa;
  }
  & p {
    font-size: 14px;
    font-weight: 400;
    margin: 4px 0 10px 0;
  }
  @media (max-width: ${SCREEN_SIZE.mobile}) {
    & label {
      font-size: 10px;
      color: #aaaaaa;
    }
    & p {
      font-size: 12px;
    }
  }
`;
