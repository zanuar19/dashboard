import React from 'react';
import { InfoGroup } from './styled';

interface props {
  label: string;
  text: string;
}

const Card = ({ label, text }: props) => {
  return (
    <InfoGroup>
      <label htmlFor={text}>{label}</label>
      <p>{text}</p>
    </InfoGroup>
  );
};

export default Card;
