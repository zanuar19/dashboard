import React, { useEffect, useState } from 'react';
import { Container, Content } from './styled';
import Header from 'components/Layout/header';
import Sidebar from 'components/Layout/sidebar';
import Loading from 'components/Loading';

interface props {
  children?: string | React.ReactNode;
  isLoading?: boolean;
}

const Layout = ({ children, isLoading }: props) => {
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    if (isOpen) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'unset';
    }
  }, [isOpen]);

  return (
    <Container isOpen={isOpen}>
      {isLoading && <Loading />}
      <Header isOpen={isOpen} onOpen={() => setIsOpen(!isOpen)} />
      <Sidebar isOpen={isOpen} />
      <Content>{children}</Content>
    </Container>
  );
};

export default Layout;
