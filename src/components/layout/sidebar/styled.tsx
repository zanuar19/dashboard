import styled from '@emotion/styled';
import { SCREEN_SIZE } from 'helpers/constants';

type ListItemProps = {
  icon?: React.ReactNode;
  children?: string | React.ReactNode;
  className?: string;
  isActive?: boolean;
};

type SidebarProps = {
  isOpen?: boolean;
};

const IconWrapper = styled.span`
  display: flex;
  margin-right: 15px;
  height: 16px;
  align-items: center;
`;

export const Sidebar = styled.div<SidebarProps>`
  grid-area: sidebar;
  @media (max-width: ${SCREEN_SIZE.mobile}) {
    position: absolute;
    background: #fff;
    width: 100%;
    top: 70px;
    height: 100%;
    z-index: 1;
    ${({ isOpen }) => !isOpen && `display: none`};
  }
`;

export const List = styled.ul`
  list-style: none;
  padding: 0;
  margin-top: 20px;
`;

const ListItemActive = `
  &:before {
    content: '';
    position: absolute;
    left: 0;
    height: 100%;
    width: 4px;
    background: #FF4646;
  }
  font-weight: 600;
  color: #FF4646;
`;

export const ListItem = styled(
  ({ icon, className, isActive, children }: ListItemProps) => (
    <li className={className}>
      {icon && <IconWrapper>{icon}</IconWrapper>}
      {children}
    </li>
  )
)<ListItemProps>`
  display: flex;
  align-items: center;
  padding: 12px 20px;
  font-size: 14px;
  cursor: pointer;
  position: relative;
  & svg {
    font-size: 20px;
  }
  ${({ isActive }) => isActive && ListItemActive};
`;
