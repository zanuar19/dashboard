import React from 'react';
import { Sidebar, List, ListItem } from './styled';
import { AiFillHome } from 'react-icons/ai';
import { HiUserCircle } from 'react-icons/hi';
import { RiCalendar2Fill } from 'react-icons/ri';

interface Props {
  isOpen: boolean;
}

const SidebarComp = ({ isOpen }: Props) => {
  return (
    <Sidebar isOpen={isOpen}>
      <List>
        <ListItem icon={<AiFillHome />}>Beranda</ListItem>
        <ListItem icon={<HiUserCircle />} isActive={true}>
          Driver Management
        </ListItem>
        <ListItem icon={<RiCalendar2Fill />}>Pickup</ListItem>
      </List>
    </Sidebar>
  );
};

export default SidebarComp;
