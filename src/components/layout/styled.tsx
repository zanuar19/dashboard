import styled from '@emotion/styled';
import { SCREEN_SIZE } from 'helpers/constants';

type ContainerProps = {
  isOpen?: boolean;
};

export const Container = styled.div<ContainerProps>`
  display: grid;
  grid-template-columns: 250px 1fr;
  grid-template-areas:
    'header header'
    'sidebar content';
  @media (max-width: ${SCREEN_SIZE.mobile}) {
    gap: 20px;
    grid-template-columns: 1fr;
    grid-template-areas:
      'header'
      'sidebar'
      'content';
    ${({ isOpen }) =>
      !isOpen &&
      `
      gap: 0;
      grid-template-areas:
    'header'
    'content';`};
  }
`;
export const Content = styled.div`
  grid-area: content;
  min-height: calc(100vh - 70px);
  background-color: #fafafa;
  padding-bottom: 30px;
`;
