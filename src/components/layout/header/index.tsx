import React from 'react';
import {
  Header,
  BrandLogo,
  HeaderUser,
  Username,
  ImgUser,
  Menu,
  BurgerMenu
} from './styled';
import logo from 'assets/logo.png';
import user from 'assets/user.png';
import { AiOutlineMenu } from 'react-icons/ai';
import { GrClose } from 'react-icons/gr';

interface Props {
  onOpen: () => void;
  isOpen: boolean;
}

const HeaderComp = ({ onOpen, isOpen }: Props) => {
  return (
    <Header>
      <Menu>
        <BurgerMenu aria-label="menu" onClick={() => onOpen()}>
          {isOpen && <GrClose />}
          {!isOpen && <AiOutlineMenu />}
        </BurgerMenu>
        <BrandLogo src={logo} alt="logo" />
      </Menu>
      <HeaderUser>
        <span>
          Hello, <Username>Shipper User</Username>
        </span>
        <ImgUser src={user} alt="user" />
      </HeaderUser>
    </Header>
  );
};

export default HeaderComp;
