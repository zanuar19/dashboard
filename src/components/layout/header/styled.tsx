import styled from '@emotion/styled';
import { SCREEN_SIZE } from 'helpers/constants';

export const Header = styled.div`
  grid-area: header;
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 70px;
  padding: 0 20px;
`;

export const Menu = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 20px;
  @media (min-width: ${SCREEN_SIZE.mobile}) {
    & > button {
      display: none;
    }
  }
`;

export const BrandLogo = styled.img`
  height: 21px;
  width: 99px;
`;

export const BurgerMenu = styled.button`
  border: none;
  background: none;
  font-size: 25px;
  color: #000;
`;

export const HeaderUser = styled.div`
  display: flex;
  align-items: center;
  font-size: 13px;
  font-weight: 400;
  @media (max-width: ${SCREEN_SIZE.mobile}) {
    & > span {
      display: none;
    }
  }
`;

export const ImgUser = styled.img`
  height: 28px;
  width: 28px;
  margin-left: 12px;
`;

export const Username = styled.span`
  color: #ff4646;
`;
