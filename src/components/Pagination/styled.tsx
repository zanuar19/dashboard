import styled from '@emotion/styled';

export const Wrapper = styled.div`
  padding: 10px 25px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  gap: 20px;
  font-size: 12px;
  font-weight: 400;
`;

type PaginateProps = {
  isDisabled?: Boolean;
};

export const Paginate = styled.button<PaginateProps>`
  border: none;
  background: none;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  padding: 10px;
  height: 30px;
  ${({ isDisabled }) => isDisabled && `color: #aaaaaa`}
`;
