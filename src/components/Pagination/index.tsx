import React, { useCallback } from 'react';
import { Wrapper, Paginate } from './styled';
import { AiOutlineLeft, AiOutlineRight } from 'react-icons/ai';

interface Props {
  onPrev: () => void;
  onNext: () => void;
  isDisabledPrev?: Boolean;
  isDisabledNext?: Boolean;
}

const Pagination = ({
  onPrev,
  onNext,
  isDisabledPrev,
  isDisabledNext
}: Props) => {
  const handlePrev = useCallback(() => {
    if (!isDisabledPrev) {
      onPrev();
    }
  }, [isDisabledPrev, onPrev]);

  const handleNext = useCallback(() => {
    if (!isDisabledNext) {
      onNext();
    }
  }, [isDisabledNext, onNext]);
  return (
    <Wrapper>
      <Paginate onClick={handlePrev} isDisabled={isDisabledPrev}>
        <AiOutlineLeft />
        Previous Page
      </Paginate>
      <Paginate onClick={handleNext} isDisabled={isDisabledNext}>
        Next Page <AiOutlineRight />
      </Paginate>
    </Wrapper>
  );
};

export default Pagination;
