import styled from '@emotion/styled';

export const Container = styled.div`
  padding: 16px 25px;
  text-align: center;
  font-weight: 800;
`;
