import React from 'react';
import { Container } from './styled';

interface Props {
  text: string;
}
const Alert = ({ text }: Props) => {
  return <Container>{text}</Container>;
};

export default Alert;
