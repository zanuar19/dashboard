# dashboard

## production

https://dashboard-mantul.web.app

## requirement

- node 14.2.0
- npm 6.14.4

## install

- npm install

## how to run

### development

```sh
npm run start
```

## features

- Each page shows 5 drivers.
- Show the next 5 users
- Show the previous 5 users.
- Disable next and prev
- Handle search
- Lazy load
- State management
- Web performance
- Service worker
- Unit Test
